import Link from 'next/link'
import style from '/styles/Head.module.css'

export default function Head() {
  return (
    <header className={style.head}><b><Link href="/">waffle</Link></b><span> | <Link href='/about'>about</Link>, <Link href="/accomplishments">accomplishments</Link>, <Link href="/qa">q&a</Link>, <Link href="/goodprojects">cool projects</Link></span></header>
  )
}
