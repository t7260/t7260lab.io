import Head from '/components/head'
import Link from 'next/link'

export default function Home() {
  return (
    <><Head />
    <title>waffle</title>
    <main>
      <h1>(1696897666771).toString(34)</h1>
      <h2>makes bad projects</h2>
      <p>hi, welcome to my poorly made (updated) site :)</p>
      <p>you <s>may want to</s> <i>should</i> visit <a href="https://absolutely-no-one.github.io/">Nobody/Mr. Incognito&rsquo;s site</a> and <a href="https://electogenius.github.io/html">Electogenius&rsquo;s site</a>, they are both interesting</p>

      <div style={{border: 'solid 3px #AEDCC0', padding: '.5em'}}>Electogenius is infinitely amazing. <b><Link href="/eIDbTM">Learn more.</Link></b></div>
    </main>
    </>
  )
}
