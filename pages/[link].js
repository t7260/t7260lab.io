import fs from 'fs';
import Router from 'next/router'
import React, { useEffect } from "react";

export default function Uselesslinkshortenerorsomething({to}) {
  useEffect(() => {
    const router = Router
    router.replace(to)
  });

  return <></>
}

export async function getStaticProps({ params }) {
  return {
    props: {
      to: JSON.parse(fs.readFileSync('./_links.json', 'utf-8'))[params.link]
    },
  }
}

export async function getStaticPaths() {
  return {
    paths: Object.keys(JSON.parse(fs.readFileSync('./_links.json', 'utf-8'))).map(v => {
      return {
        params: {
          link: v
        }
      }
    }),
    fallback: false
  };
}
